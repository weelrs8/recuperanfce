<?php

namespace App\Http\Controllers;

use App\Key;
use Illuminate\Http\Request;

use App\Jobs\RecuperaNfce;
use App\Task;
use App\Http\Requests;

class NfceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $tasks = Task::where('user_id', \Auth::user()->id)->get();

        $step = [];
        $status = [];
        $error = [];
        foreach($tasks as $t) {
            $step[$t->id]['step']   = Key::where(['task_id' => $t->id, 'status' => 3])->count();
            $step[$t->id]['total']  = Key::where('task_id', $t->id)->count();
            $error[$t->id] = Key::where(['task_id' => $t->id, 'status' => 2])->count();
            $waiting = Key::where(['task_id' => $t->id, 'status' => 0])->count();

            if ($waiting == $step[$t->id]['total']) {
                $status[$t->id] = 'Aguardando';
            } else {
                if ($step[$t->id]['step'] == $step[$t->id]['total']) {
                    $status[$t->id] = 'Concluido';
                } else {
                    $calc = $step[$t->id]['total'] - $error[$t->id];
                    if ($calc === $step[$t->id]['step'])
                        $status[$t->id] = sprintf('Concluido [%s falhas]', $error[$t->id]);
                    else
                        $status[$t->id] = 'Em andamento';
                }
            }
        }

        return
            view('dashboard.nfce.home')
                ->with([
                    'tasks' => $tasks,
                    'step'  => $step,
                    'status'=> $status,
                    'errors'=> $error
                ]);
    }

    public function new($uf = null, $modo = null)
    {
        return
            view('dashboard.nfce.add')
                ->with([
                    'uf' => $uf,
                    'modo' => $modo
                ]);
    }

    public function add(Request $request)
    {
        $file = $request->file('file');

        if ($request->hasFile('file') && $file->getMimeType() == 'application/vnd.ms-office') {
            $iKeys = $this->parseSheet($file->getRealPath());
        } else {
            $iKeys = $request->input('keys');
            $iKeys = preg_replace("/[^0-9]/", "", explode("\r\n", $iKeys));
        }

        $validator1 = \Validator::make($request->all(), [
            'name' => 'required|min:3|max:80'
        ]);

        $iName = filter_var($request->input('name'), FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        $k = [];
        foreach($iKeys as $key) {
            if (substr($key, 20, 2) != 65)
                continue;

            if (strlen($key) === 44)
                $k[] = $key;
        }

        if (count($k) < 1)
            $validator2 = \Validator::make(['keys' => null], ['keys' => 'required|min:44']);

        if ($validator1->fails())
            return redirect('nfce/new')->withErrors($validator1)->withInput();
        if (isset($validator2) && $validator2->fails())
            return redirect('nfce/new')->withErrors($validator2)->withInput();

        $task = Task::create([
            'user_id' => \Auth::user()->id,
            'name' => $iName
        ]);

        foreach($k as $v)
            $task->keys()->create([
                'key' => $v
            ]);

        $num1 = $task->keys()->where('status', 0)->count();
        $num2 = 9;

        $key = $task->keys()->select('id', 'key')->where('status', 0)->get();

        $keys = [];
        foreach($key as $v)
            $keys[] = ['id' => $v->id, 'key' => $v->key];

        $d = (int)($num1 / $num2);
        for ($i = 0; $i < 9; $i++) {
            if ($d > 0)
                $this->dispatch(
                    new RecuperaNfce($task->id, array_splice($keys, 0, $d))
                );
        }

        $r = $num1 % $num2;
        if ($r > 0)
            $this->dispatch(
                new RecuperaNfce($task->id, array_splice($keys, 0, $r))
            );

        $request->session()->flash('status', 'Tarefa agendada com sucesso!');

        return redirect('nfce');
    }

    public function keys($id)
    {
        $keys = Task::where(['user_id' => \Auth::user()->id])->find($id);

        return
            view('dashboard.nfce.keys')
                ->with(
                    'keys',
                    $keys->keys()->select('id', 'key', 'attempts', 'status', 'created_at', 'updated_at')->get()
                );
    }

    public function d1($id)
    {
        $task = Task::where('user_id', \Auth::user()->id)->find($id);

        $filename = time() . preg_replace("/[^0-9a-zA-Z]/", "-", $task->name);
        $filepath = sprintf("%s/%s.zip", storage_path(), $filename);

        if (file_exists($filepath))
            return
                response()
                    ->download(
                        $filepath,
                        $filename . ".zip",
                        [
                            'Content-type' => 'application/zip',
                            'Content-length' => filesize($filepath),
                        ]
                    );

        $zip = new \ZipArchive();
        if ($zip->open($filepath, \ZipArchive::CREATE) !== TRUE)
            return redirect('nfce');

        foreach ($task->keys()->get() as $k) {
            if (strlen($k->source) > 0)
                $zip->addFromString($k->key . ".xml", $k->source);
        }

        if (!$zip->numFiles)
            return
                redirect('nfce')
                    ->with('failure', 'Não é possível baixar tarefa, pois a mesma não possuí chaves concluidas');

        $zip->close();

        return
            response()
                ->download(
                    $filepath,
                    $filename . ".zip",
                    [
                        'Content-type' => 'application/zip',
                        'Content-length' => filesize($filepath),
                    ]
                );
    }

    public function d2()
    {

    }

    public function drop($id)
    {
        $task = Task::where(['user_id' => \Auth::user()->id, 'id' => $id])->delete();

        if ($task)
            return redirect('nfce')->with('success', 'Tarefa removida com sucesso!');
        else
            return redirect('nfce')->with('failure', 'Falha ao remover tarefa!');
    }

    private function parseSheet($file)
    {
        $e = \PHPExcel_IOFactory::createReaderForFile($file);
        $e->setReadDataOnly();
        $o = $e->load($file);
        $c = $o->getActiveSheet()->toArray(null, true,true,true);

        $b = array_splice($c, 8);

        $k = [];
        foreach ($b as $x) {
            $k[] = preg_replace("/[^0-9]/", "", $x['D']);
        }

        return $k;
    }
}
