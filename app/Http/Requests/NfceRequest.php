<?php

namespace App\Http\Requests;

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;

class NfceRequest
{
    private static $baseUri = 'https://www.sefaz.mt.gov.br/';

    public static function get($key)
    {
        try {
            if (strlen($key) != 44)
                return ['status' => false, 'message' => 'invalid key']; //throw new \Exception("invalid key", $key);

            if (substr($key, 20, 2) != 65)
                return ['status' => false, 'message' => 'invalid key, it is not a nfce", $key']; //throw new \Exception("invalid key, it is not a nfce", $key);

            $client = new Client([
                'base_uri' => self::$baseUri
            ]);

            $jar = new CookieJar();

            $pag = $client->request(
                'GET',
                '/nfce/consultanfce',
                [
                    'query' =>
                        [
                            'chNFe' => $key,
                            'nVersao' => 100, //100
                            'tpAmb' => 1, //1
                            'dhEmi' => '99999999999999999999999999999999999999999999999999', //'323031362D30332D32325430393A30343A30322D30343A3030',
                            'vNF' => 0.00,
                            'vICMS' => 0.00,
                            'digVal' => '99999999999999999999999999999999999999999999999999999999',
                            'cIdToken' => '000001',
                            'cHashQRCode' => '9999999999999999999999999999999999999999'
                        ],
                    'verify' => false,
                    'cookies' => $jar
                ]
            );

            libxml_use_internal_errors(true);

            if ($pag->getStatusCode() != 200)
                return ['status' => false, 'message' => 'request failure or page is offline']; //throw new \Exception("request failure or page is offline", $key);

            $dom = new \DOMDocument();
            $content1 = $pag->getBody()->getContents();
            if (empty($content1))
                return ['status' => false, 'message' => 'first request, no content'];
            $dom->loadHTML($content1);

            if ($dom->getElementById("imgInvalido1") == null)
                return ['status' => false, 'message' => 'invalid key or contingency']; //throw new \Exception("invalid key or contingency", $key);

            unset($dom);

            $output = $client->request(
                'GET',
                '/nfce/consultanfce',
                [
                    'query' =>
                        [
                            'pagn' => 'impressao',
                            'tpImp' => 'COMPLETO'
                        ],
                    'verify' => false,
                    'cookies' => $jar
                ]
            );

            if ($output->getStatusCode() != 200)
                return ['status' => false, 'message' => 'request failure or page is offline']; //throw new \Exception("request failure or page is offline", $key);

            $content2 = $output->getBody()->getContents();

            $dom = new \DOMDocument();
            if (empty($content2))
                return ['status' => false, 'message' => 'second request, no content'];
            $dom->loadHTML($content2);

            if ($dom->getElementById("NFe") == null)
                return ['status' => false, 'message' => 'request failure']; //throw new \Exception("request failure", $key);

            unset($dom);

            return ['status' => true, 'content' => $content2];
        } catch (\Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }
}