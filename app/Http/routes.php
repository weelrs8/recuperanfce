<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::group(['prefix' => 'nfce'], function() {
    Route::get('/', 'NfceController@index');
    Route::get('new', 'NfceController@new');
    Route::get('new/{uf}/{modo}', 'NfceController@new');
    Route::get('keys/{id}', 'NfceController@keys');
    Route::post('add', 'NfceController@add');
    Route::get('file/{id}', 'NfceController@d1');
    Route::get('d2/{key}', 'NfceController@d2');
    Route::delete('file/{id}', 'NfceController@drop');
});
