<?php

namespace App\Jobs;

use App\Key;
use App\Task;
use App\Traits\NfceParse;
use App\Http\Requests\NfceRequest;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RecuperaNfce extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, NfceParse;

    private $id;
    private $keys;
    private $fails = [];

    /**
     * Create a new job instance.
     */
    public function __construct($id, $keys)
    {
        $this->id   = $id;
        $this->keys = $keys;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $debug = true;

        if ($debug)
            echo "Iniciando Trabalho \n";

        if ($debug)
            echo sprintf("Encontramos %s tarefas para serem processadas. \n", count($this->keys));

        $keys = [];
        foreach ($this->keys as $k)
            $keys[$k['id']] = ['key' => $k['key']];

        $chk = Key::where('status', 3)->whereIn('id', array_keys($keys))->count();

        if ($chk > 0) {
            if ($debug)
                echo sprintf("Tarefa: %s ja foi processada \n", implode(", ", array_keys($keys)));

            return;
        }

        Key::whereIn('id', array_keys($keys))->update(['status' => 1]);

        if ($debug)
            echo sprintf("Mudamos o status das seguintes tarefas: %s \n", implode(", ", array_keys($keys)));

        $run = function($keys) use ($debug) {
            foreach($keys as $k => $v) {
                if ($debug)
                    echo sprintf("Iniciando processo tarefa: %s \n", $k);

                $req = NfceRequest::get($v['key']);

                $chk = Key::where(['id' => $k, 'status' => 3])->count();

                if ($chk > 0) {
                    if ($debug)
                        echo sprintf("Tarefa: %s ja foi processada \n", $k);

                    continue;
                }

                if (!$req['status']) {
                    $this->fails[$k] = ['key' => $v['key'], 'attempts' => isset($this->fails[$k]['attempts']) ? $this->fails[$k]['attempts'] + 1 : 1];

                    if ($this->fails[$k]['attempts'] >= 5) {
                        Key::where('id', $k)
                            ->update([
                                'status' => 2,
                                'attempts' => $this->fails[$k]['attempts']
                            ]);
                        unset($this->fails[$k]);
                    } else {
                        Key::where('id', $k)
                            ->update([
                                'attempts' => $this->fails[$k]['attempts']
                            ]);
                    }

                    if ($debug)
                        echo sprintf("Falha na tarefa: %s \n Exception: %s \n", $k, $req['message']);

                    continue;
                }

                if ($req['status'] && $req['content']) {
                    $this->load($req['content']);

                    Key::where('id', $k)
                        ->update([
                            'status'    => 3,
                            'source'    => $this->getNfce()
                        ]);

                    Task::where('id', $this->id)->increment('status', 1);

                    unset($this->fails[$k]);

                    if ($debug)
                        echo sprintf("Tarefa: %s concluida \n", $k);
                }
            }
        };

        $run($keys);

        for ($i = 0; $i <= 5; $i++) {
            if ($debug)
                echo sprintf("Verificando por tarefas que falharam #%s \n", $i);

            $fail = count($this->fails);
            if ($fail > 0) {
                if ($debug)
                    echo sprintf("Encontramos %s falhas. \n", $fail) ;

                $run($this->fails);
            }
        }

        if ($debug)
            echo "Processo concluido. \n";
    }
}
