<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Key extends Model
{
    protected $table = 'keys';
    protected $fillable = [
        'key', 'attempts', 'status', 'source',
    ];

    public function tasks()
    {
        return $this->belongsTo(Task::class);
    }
}
