<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Task extends Model
{
    protected $table = 'tasks';
    protected $fillable = [
        'user_id', 'name', 'status',
    ];

    public function keys()
    {
        return $this->hasMany(Key::class);
    }
}
