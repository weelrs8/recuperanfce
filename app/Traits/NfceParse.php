<?php

namespace App\Traits;

use Wa72\HtmlPageDom\HtmlPage;

trait NfceParse
{
//ide
    public $nNF;
    public $natOp;
    public $mod;
    public $serie;
    public $chNFe;
    public $verAplic;
    public $dhEmi;
    public $dhSaida;
    public $procEmi;
    public $verProc;
    public $tpEmis;
    public $finNFe;
    public $tpNF;
    public $indPag;
    public $idDest;
    public $indFinal;
    public $indPres;
    public $tpImp;
    public $cDV;
    public $tpAmb;
    public $dhCont;
    public $xJust;

    //emitente
    public $cnpj;
    public $xNome;
    public $xFant;
    public $xLgr;
    public $nro;
    public $xBairro;
    public $cMun;
    public $xMun;
    public $uf;
    public $cep;
    public $cPais;
    public $xPais;
    public $fone;
    public $ie;
    public $crt;
    public $cMunFG;

    //destinatario
    public $dxNome;
    public $dCpf;
    public $indIEDest;

    public $produtos = [];

    //totais
    public $vBC;
    public $vICMS;
    public $vICMSDeson;
    public $vBCST;
    public $vST;
    public $vProd;
    public $vFrete;
    public $vSeg;
    public $vDesc;
    public $vII;
    public $vIPI;
    public $vPIS;
    public $vCOFINS;
    public $vOutro;
    public $vNF;
    public $vTotTrib;

    //infProt
    public $digVal;
    public $xMotivo;
    public $nProt;
    public $dhRecbto;

    //transporte
    public $modFrete;

    //forma de pagamento
    public $tPag;
    public $vPag;

    //informacoes complementares
    public $infCpl;

    private $dom;
    private $html;
    private $page;

    public function load($page)
    {
        $this->dom = new \DOMDocument("1.0", "UTF-8");
        $this->dom->formatOutput = false;

        $this->page = $page;
        $this->html = new HtmlPage($page);
    }

    private function pNFe()
    {
        $e = $this->html->filter('#NFe');

        $c = $this->html->filter('table');

        $this->chNFe    = preg_replace("/[^0-9]/", "", $c->filter('span')->getNode(0)->nodeValue);
        $this->mod      = $e->filter('span')->getNode(0)->nodeValue;
        $this->serie    = $e->filter('span')->getNode(1)->nodeValue;
        $this->nNF      = $e->filter('span')->getNode(2)->nodeValue;

        $dhEmi = \DateTime::createFromFormat(
            "d/m/Y H:i:sP", preg_replace('/[^0-9+\-\/:]+/',
                ' ',
                $e->filter('span')->getNode(3)->nodeValue)
        );

        $this->dhEmi = $dhEmi->format("Y-m-d\\TH:i:sP");

        if ($e->filter('span')->getNode(4)->nodeValue != "") {
            $dhSaida = \DateTime::createFromFormat(
                "d/m/Y H:i:sP", preg_replace('/[^0-9+\-\/:]+/',
                    ' ',
                    $e->filter('span')->getNode(4)->nodeValue)
            );

            $this->dhSaida = $dhSaida->format("Y-m-d\\TH:i:sP");
        } else {
            $this->dhSaida = "";
        }

        $this->vNF = str_replace([".", ","], ["", "."], $e->filter('span')->getNode(5)->nodeValue);

        if ($e->filter('fieldset')->filter('legend')->getNode(2)->nodeValue == 'Destinatrio') {
            $this->procEmi  = preg_replace("/[^0-9]/", "", $e->filter('span')->getNode(17)->nodeValue);
            $this->verProc  = preg_replace("/[^0-9]/", "", $e->filter('span')->getNode(18)->nodeValue);
            $this->tpEmis   = preg_replace("/[^0-9]/", "", $e->filter('span')->getNode(19)->nodeValue);
            $this->finNFe   = preg_replace("/[^0-9]/", "", $e->filter('span')->getNode(20)->nodeValue);

            $this->natOp    = $e->filter('span')->getNode(21)->nodeValue;
            $this->tpNF     = preg_replace("/[^0-9]/", "", $e->filter('span')->getNode(22)->nodeValue);
            $this->indPag   = preg_replace("/[^0-9]/", "", $e->filter('span')->getNode(23)->nodeValue);

            $this->digVal   = $e->filter('span')->getNode(24)->nodeValue;

            $evento = trim($e->filter('span')->getNode(25)->nodeValue);
            $this->xMotivo  = ($evento == 'Autorização de Uso') ? "Autorizado o uso da NF-e" : $evento;

            $this->nProt    = $e->filter('span')->getNode(26)->nodeValue;
            $this->cDV      = substr($this->chNFe, -1);

            $dhRec = \DateTime::createFromFormat(
                "d/m/Y H:i:sP", preg_replace('/[^0-9+\-\/:]+/',
                    ' ',
                    $e->filter('span')->getNode(27)->nodeValue)
            );

            $this->dhRecbto = $dhRec->format("Y-m-d\\TH:i:sP");
        } else {
            $this->procEmi  = preg_replace("/[^0-9]/", "", $e->filter('span')->getNode(10)->nodeValue);
            $this->verProc  = preg_replace("/[^0-9]/", "", $e->filter('span')->getNode(11)->nodeValue);
            $this->tpEmis   = preg_replace("/[^0-9]/", "", $e->filter('span')->getNode(12)->nodeValue);
            $this->finNFe   = preg_replace("/[^0-9]/", "", $e->filter('span')->getNode(13)->nodeValue);

            $this->natOp    = $e->filter('span')->getNode(14)->nodeValue;
            $this->tpNF     = preg_replace("/[^0-9]/", "", $e->filter('span')->getNode(15)->nodeValue);
            $this->indPag   = preg_replace("/[^0-9]/", "", $e->filter('span')->getNode(16)->nodeValue);

            $this->digVal   = $e->filter('span')->getNode(17)->nodeValue;

            $evento = trim($e->filter('span')->getNode(18)->nodeValue);
            $this->xMotivo  = ($evento == 'Autorização de Uso') ? "Autorizado o uso da NF-e" : $evento;

            $this->nProt    = $e->filter('span')->getNode(19)->nodeValue;
            $this->cDV      = substr($this->chNFe, -1);

            $dhRec = \DateTime::createFromFormat(
                "d/m/Y H:i:sP", preg_replace('/[^0-9+\-\/:]+/',
                    ' ',
                    $e->filter('span')->getNode(20)->nodeValue)
            );

            $this->dhRecbto = $dhRec->format("Y-m-d\\TH:i:sP");
        }



        $this->idDest   = "1";
        $this->indFinal = "1";
        $this->indPres  = "1";
        $this->tpImp    = "4";
        $this->verAplic = "3.10";
        $this->tpAmb    = "1";
    }

    private function pEmitente()
    {
        $e = $this->html->filter('#Emitente')->filter('span');

        if (@!$e->getNode(0)->nodeValue || @empty($e->getNode(0)->nodeValue))
            throw new \Exception("Falha ao recuperar XML");

        $this->xNome    = $e->getNode(0)->nodeValue;
        $this->xFant    = $e->getNode(1)->nodeValue;
        $this->cnpj     = preg_replace("/[^0-9]/", "", $e->getNode(2)->nodeValue);

        $this->xLgr     = substr(trim($e->getNode(3)->nodeValue), 0, -32);
        $this->nro      = "SEM NUMERO";
        $this->xBairro  = $e->getNode(4)->nodeValue;
        $this->cMun     = preg_replace("/[^0-9]/", "", $e->getNode(6)->nodeValue);
        $this->cMunFG     = preg_replace("/[^0-9]/", "", $e->getNode(6)->nodeValue);
        $this->xMun     = trim(preg_replace("/[^a-zA-Z ]/", "", $e->getNode(6)->nodeValue));
        $this->uf       = $e->getNode(8)->nodeValue;
        $this->cep      = str_replace("-", "", $e->getNode(5)->nodeValue);
        $this->cPais    = preg_replace("/[^0-9]/", "", $e->getNode(9)->nodeValue);
        $this->xPais    = preg_replace("/[^a-zA-Z]/", "", $e->getNode(9)->nodeValue);
        $this->fone     = preg_replace("/[^0-9]/", "", $e->getNode(7)->nodeValue);

        $this->ie       = preg_replace("/[^0-9]/", "", $e->getNode(10)->nodeValue);
        $this->crt      = preg_replace("/[^0-9]/", "", $e->getNode(15)->nodeValue);
    }

    private function pDestinatario()
    {
        $e = $this->html->filter('#DestRem')->filter('span');

        $this->dxNome    = $e->getNode(0)->nodeValue;
        $this->dCpf      = preg_replace("/[^0-9]/", "", $e->getNode(1)->nodeValue);
        $this->indIEDest = (string) intval(preg_replace("/[^0-9]/", "", $e->getNode(9)->nodeValue));
    }

    private function pProdutos()
    {
        $tProduto = count($p = $this->html->filterXPath("//table[contains(@id,'A + ')]"));

        if (!$tProduto)
            throw new \Exception("Não foi possível localizar os dados de produtos!");

        for ($x = 1; $x <= $tProduto; $x++)
        {
            $t = $this->html->filterXPath("//*[@id='$x']")->filter('span');
            $p = $this->html->filterXPath("//*[@id='A + $x']")->filter('span');

            $this->produtos[] = [
                //produto
                'cProd'     => $t->getNode(0)->nodeValue,
                'cEAN'      => $t->getNode(10)->nodeValue,
                'xProd'     => $p->getNode(1)->nodeValue,
                'ncm'       => $t->getNode(1)->nodeValue,
                'cest'      => $t->getNode(2)->nodeValue,
                'cfop'      => $t->getNode(4)->nodeValue,
                'uCom'      => $t->getNode(11)->nodeValue,
                'qCom'      => str_replace(['.', ','], ['', '.'], $t->getNode(12)->nodeValue),
                'vUnCom'    => str_replace(['.', ','], ['', '.'], $t->getNode(16)->nodeValue),
                'vProd'     => str_replace(['.', ','], ['', '.'], $p->getNode(4)->nodeValue),
                'cEANTrib'  => $t->getNode(13)->nodeValue,
                'uTrib'     => $t->getNode(14)->nodeValue,
                'qTrib'     => str_replace(['.', ','], ['', '.'], $t->getNode(15)->nodeValue),
                'vUnTrib'   => str_replace(['.', ','], ['', '.'], $t->getNode(17)->nodeValue),
                'indTot'    => preg_replace("/[^0-9]/", "", $t->getNode(9)->nodeValue),

                //imposto
                'vTotTrib'  => ($t->getNode(20)->nodeValue == "") ? '0.00' : $t->getNode(20)->nodeValue,
                'orig'      => preg_replace("/[^0-9]/", "", $t->getNode(22)->nodeValue),
                'csosn'     => preg_replace("/[^0-9]/", "", $t->getNode(23)->nodeValue)
            ];
        }
    }

    private function pTotais()
    {
        $e = $this->html->filter('#Totais')->filter('span');

        if (!$e)
            throw new \Exception('Não foi possível localizar os Totais');

        $this->vBC          = str_replace([".", ","], ["", "."], $e->getNode(0)->nodeValue);
        $this->vICMS        = str_replace([".", ","], ["", "."], $e->getNode(1)->nodeValue);
        $this->vICMSDeson   = str_replace([".", ","], ["", "."], $e->getNode(2)->nodeValue);
        $this->vBCST        = str_replace([".", ","], ["", "."], $e->getNode(3)->nodeValue);
        $this->vST          = str_replace([".", ","], ["", "."], $e->getNode(4)->nodeValue);
        $this->vProd        = str_replace([".", ","], ["", "."], $e->getNode(5)->nodeValue);
        $this->vFrete       = str_replace([".", ","], ["", "."], $e->getNode(6)->nodeValue);
        $this->vSeg         = str_replace([".", ","], ["", "."], $e->getNode(7)->nodeValue);
        $this->vDesc        = str_replace([".", ","], ["", "."], $e->getNode(11)->nodeValue);
        $this->vII          = str_replace([".", ","], ["", "."], $e->getNode(12)->nodeValue);
        $this->vIPI         = str_replace([".", ","], ["", "."], $e->getNode(9)->nodeValue);
        $this->vPIS         = str_replace([".", ","], ["", "."], $e->getNode(13)->nodeValue);
        $this->vCOFINS      = str_replace([".", ","], ["", "."], $e->getNode(14)->nodeValue);
        $this->vOutro       = str_replace([".", ","], ["", "."], $e->getNode(8)->nodeValue);
        $this->vTotTrib     = str_replace([".", ","], ["", "."], $e->getNode(15)->nodeValue);
    }

    private function pTransporte()
    {
        $e = $this->html->filter('#Transporte')->filter('span');

        if (!$e)
            throw new \Exception('Não foi possível localizar o Transporte');

        $this->modFrete = preg_replace("/[^0-9]/", "", $e->getNode(0)->nodeValue);
    }

    private function pCobranca()
    {
        $e = $this->html->filter('#Cobranca')->filter('span');

        if (!$e)
            throw new \Exception('Não foi possível localizar a Cobranca');

        $this->tPag = preg_replace("/[^0-9]/", "", $e->getNode(0)->nodeValue);
        $this->vPag = str_replace([".", ","], ["", "."], $e->getNode(1)->nodeValue);
    }

    private function pInfo()
    {
        $e = $this->html->filter('#Inf');

        if (!$e)
            throw new \Exception('Não foi possível localizar as Informações');

        if ($this->tpEmis == 9) {
            $this->xJust = "Danfe em contingencia impresso em decorrencia de problemas tecnicos.";
            $this->dhCont = $this->dhEmi;
        }

        $infCpl1 = trim(strstr(@$e->filter('fieldset')->getNode(1)->nodeValue, 'http://'));
        $infCpl2 = trim(strstr(@$e->filter('fieldset')->getNode(0)->nodeValue, 'http://'));

        $this->infCpl = ($infCpl1) ? $infCpl1 : $infCpl2;
    }

    private function getCodigoNF($nNF)
    {
        $qtd = 8 - strlen($nNF);
        $z = "";

        for ($i = 0; $i < $qtd; $i++)
        {
            $z .= "0";
        }

        return (string) $z . $nNF;
    }

    private function getCodigoEstado($uf)
    {
        switch($uf)
        {
            case 'RO': return '11';
            case 'AC': return '12';
            case 'AM': return '13';
            case 'RR': return '14';
            case 'PA': return '15';
            case 'AP': return '16';
            case 'TO': return '17';

            case 'MA': return '21';
            case 'PI': return '22';
            case 'CE': return '23';
            case 'RN': return '24';
            case 'PB': return '25';
            case 'PE': return '26';
            case 'AL': return '27';
            case 'SE': return '28';
            case 'BA': return '29';

            case 'MG': return '31';
            case 'ES': return '32';
            case 'RJ': return '33';
            case 'SP': return '35';

            case 'PR': return '41';
            case 'SC': return '42';
            case 'RS': return '43';

            case 'MS': return '50';
            case 'MT': return '51';
            case 'GO': return '52';
            case 'DF': return '53';
        }
    }

    private function parse()
    {
        $nfeProc = $this->dom->createElement("nfeProc");
        $nfeProc->setAttribute("versao", "3.10");

        $nfeProcNS = $this->dom->createAttribute("xmlns");
        $nfeProcNS->value = "http://www.portalfiscal.inf.br/nfe";
        $nfeProc->appendChild($nfeProcNS);

        $nfe = $this->dom->createElement("NFe");

        $nsNFe = $this->dom->createAttribute("xmlns");
        $nsNFe->value = "http://www.portalfiscal.inf.br/nfe";

        $nfe->appendChild($nsNFe);

        $chNFe = $this->html->filter('table')->filter('span');

        $infNFe = $this->dom->createElement("infNFe");
        $infNFe->setAttribute("versao", "3.10");
        $infNFe->setAttribute("Id", "NFe" . preg_replace("/[^0-9]/", "", $chNFe->getNode(0)->nodeValue));

        $ide = $this->dom->createElement("ide");

        $infNFe->appendChild($ide);

        $cUF        = $this->dom->createElement("cUF", $this->getCodigoEstado($this->uf));
        $ide->appendChild($cUF);
        $cNF        = $this->dom->createElement("cNF", $this->getCodigoNF($this->nNF));
        $ide->appendChild($cNF);
        $natOp      = $this->dom->createElement("natOp", $this->natOp);
        $ide->appendChild($natOp);
        $indPag     = $this->dom->createElement("indPag", $this->indPag);
        $ide->appendChild($indPag);
        $mod        = $this->dom->createElement("mod", $this->mod);
        $ide->appendChild($mod);
        $serie      = $this->dom->createElement("serie", $this->serie);
        $ide->appendChild($serie);
        $nNF        = $this->dom->createElement("nNF", $this->nNF);
        $ide->appendChild($nNF);

        //$dhEmi2 = new \DateTime($this->dhEmi);

        //$dhEmi      = $this->dom->createElement("dhEmi", $dhEmi2->format("Y-d-m\\TH:i:sP"));
        $dhEmi      = $this->dom->createElement("dhEmi", $this->dhEmi);
        $ide->appendChild($dhEmi);
        $tpNF       = $this->dom->createElement("tpNF", $this->tpNF);
        $ide->appendChild($tpNF);
        $idDest     = $this->dom->createElement("idDest", $this->idDest);
        $ide->appendChild($idDest);
        $cMunFG     = $this->dom->createElement("cMunFG", $this->cMunFG);
        $ide->appendChild($cMunFG);
        $tpImp      = $this->dom->createElement("tpImp", $this->tpImp);
        $ide->appendChild($tpImp);
        $tpEmis     = $this->dom->createElement("tpEmis", $this->tpEmis);
        $ide->appendChild($tpEmis);
        $cDV        = $this->dom->createElement("cDV", $this->cDV);
        $ide->appendChild($cDV);
        $tpAmb      = $this->dom->createElement("tpAmb", $this->tpAmb);
        $ide->appendChild($tpAmb);
        $finNFe     = $this->dom->createElement("finNFe", $this->finNFe);
        $ide->appendChild($finNFe);
        $indFinal   = $this->dom->createElement("indFinal", $this->indFinal);
        $ide->appendChild($indFinal);
        $indPres    = $this->dom->createElement("indPres", $this->indPres);
        $ide->appendChild($indPres);
        $procEmi    = $this->dom->createElement("procEmi", $this->procEmi);
        $ide->appendChild($procEmi);
        $verProc    = $this->dom->createElement("verProc", $this->verProc);
        $ide->appendChild($verProc);

        if ($this->tpEmis == "9") {
            $dhCont     = $this->dom->createElement("dhCont", $this->dhCont);
            $ide->appendChild($dhCont);
            $xJust      = $this->dom->createElement("xJust", $this->xJust);
            $ide->appendChild($xJust);
        }

        $emit = $this->dom->createElement("emit");
        $infNFe->appendChild($emit);

        $cnpj       = $this->dom->createElement("CNPJ", $this->cnpj);
        $emit->appendChild($cnpj);
        $xNome      = $this->dom->createElement("xNome", $this->xNome);
        $emit->appendChild($xNome);
        $xFant      = $this->dom->createElement("xFant", $this->xFant);
        $emit->appendChild($xFant);

        $eEmit      = $this->dom->createElement("enderEmit");
        $emit->appendChild($eEmit);

        $xLgr       = $this->dom->createElement("xLgr", $this->xLgr);
        $eEmit->appendChild($xLgr);
        $nro        = $this->dom->createElement("nro", "SEM NUMERO");
        $eEmit->appendChild($nro);
        $xBairro    = $this->dom->createElement("xBairro", $this->xBairro);
        $eEmit->appendChild($xBairro);
        $cMun       = $this->dom->createElement("cMun", $this->cMun);
        $eEmit->appendChild($cMun);
        $xMun       = $this->dom->createElement("xMun", $this->xMun);
        $eEmit->appendChild($xMun);
        $uf         = $this->dom->createElement("UF", $this->uf);
        $eEmit->appendChild($uf);
        $cep        = $this->dom->createElement("CEP", $this->cep);
        $eEmit->appendChild($cep);
        $cPais      = $this->dom->createElement("cPais", $this->cPais);
        $eEmit->appendChild($cPais);
        $xPais      = $this->dom->createElement("xPais", $this->xPais);
        $eEmit->appendChild($xPais);
        $fone       = $this->dom->createElement("fone", $this->fone);
        $eEmit->appendChild($fone);
        $ie         = $this->dom->createElement("IE", $this->ie);
        $emit->appendChild($ie);
        $crt        = $this->dom->createElement("CRT", $this->crt);
        $emit->appendChild($crt);

        if ($this->indIEDest == 9) {
            $dest       = $this->dom->createElement("dest");
            $infNFe->appendChild($dest);

            $dCpf       = $this->dom->createElement("CPF", $this->dCpf);
            $dest->appendChild($dCpf);
            $dxNome     = $this->dom->createElement("xNome", $this->dxNome);
            $dest->appendChild($dxNome);
            $dIE        = $this->dom->createElement("indIEDest", $this->indIEDest);
            $dest->appendChild($dIE);
        }


        for($i = 0; $i < count($this->produtos); $i++)
        {
            $det = $this->dom->createElement("det");
            $det->setAttribute("nItem", $i + 1);

            $prod = $this->dom->createElement("prod");

            $cProd      = $this->dom->createElement("cProd", $this->produtos[$i]['cProd']);
            $prod->appendChild($cProd);
            $cEAN       = $this->dom->createElement("cEAN", $this->produtos[$i]['cEAN']);
            $prod->appendChild($cEAN);
            $xProd      = $this->dom->createElement("xProd", $this->produtos[$i]['xProd']);
            $prod->appendChild($xProd);
            $ncm        = $this->dom->createElement("NCM", $this->produtos[$i]['ncm']);
            $prod->appendChild($ncm);
            $cfop       = $this->dom->createElement("CFOP", $this->produtos[$i]['cfop']);
            $prod->appendChild($cfop);
            $uCom       = $this->dom->createElement("uCom", $this->produtos[$i]['uCom']);
            $prod->appendChild($uCom);
            $qCom       = $this->dom->createElement("qCom", $this->produtos[$i]['qCom']);
            $prod->appendChild($qCom);
            $vUnCom     = $this->dom->createElement("vUnCom", $this->produtos[$i]['vUnCom']);
            $prod->appendChild($vUnCom);
            $vProd      = $this->dom->createElement("vProd", $this->produtos[$i]['vProd']);
            $prod->appendChild($vProd);
            $cEANTrib   = $this->dom->createElement("cEANTrib", $this->produtos[$i]['cEANTrib']);
            $prod->appendChild($cEANTrib);
            $uTrib      = $this->dom->createElement("uTrib", $this->produtos[$i]['uTrib']);
            $prod->appendChild($uTrib);
            $qTrib      = $this->dom->createElement("qTrib", $this->produtos[$i]['qTrib']);
            $prod->appendChild($qTrib);
            $vUnTrib    = $this->dom->createElement("vUnTrib", $this->produtos[$i]['vUnTrib']);
            $prod->appendChild($vUnTrib);
            $indTot     = $this->dom->createElement("indTot", $this->produtos[$i]['indTot']);
            $prod->appendChild($indTot);

            $imposto    = $this->dom->createElement("imposto");

            $icms       = $this->dom->createElement("ICMS");
            $imposto->appendChild($icms);
            $icmssn900  = $this->dom->createElement("ICMSSN900");
            $icms->appendChild($icmssn900);
            $orig       = $this->dom->createElement("orig", $this->produtos[$i]['orig']);
            $icmssn900->appendChild($orig);
            $csosn      = $this->dom->createElement("CSOSN", $this->produtos[$i]['csosn']);
            $icmssn900->appendChild($csosn);

            $det->appendChild($prod);
            $det->appendChild($imposto);

            $infNFe->appendChild($det);
        }

        $total      = $this->dom->createElement("total");
        $infNFe->appendChild($total);
        $icmstot    = $this->dom->createElement("ICMSTot");
        $total->appendChild($icmstot);
        $vBC3       = $this->dom->createElement("vBC", $this->vBC);
        $icmstot->appendChild($vBC3);
        $vICMS      = $this->dom->createElement("vICMS", $this->vICMS);
        $icmstot->appendChild($vICMS);
        $vICMSDeson = $this->dom->createElement("vICMSDeson", $this->vICMSDeson);
        $icmstot->appendChild($vICMSDeson);
        $vBCST      = $this->dom->createElement("vBCST", $this->vBCST);
        $icmstot->appendChild($vBCST);
        $vST        = $this->dom->createElement("vST", $this->vST);
        $icmstot->appendChild($vST);
        $vProd2     = $this->dom->createElement("vProd", $this->vProd);
        $icmstot->appendChild($vProd2);
        $vFrete     = $this->dom->createElement("vFrete", $this->vFrete);
        $icmstot->appendChild($vFrete);
        $vSeg       = $this->dom->createElement("vSeg", $this->vSeg);
        $icmstot->appendChild($vSeg);
        $vDesc      = $this->dom->createElement("vDesc", $this->vDesc);
        $icmstot->appendChild($vDesc);
        $vII        = $this->dom->createElement("vII", $this->vII);
        $icmstot->appendChild($vII);
        $vIPI       = $this->dom->createElement("vIPI", $this->vIPI);
        $icmstot->appendChild($vIPI);
        $vPIS2      = $this->dom->createElement("vPIS", $this->vPIS);
        $icmstot->appendChild($vPIS2);
        $vCOFINS3   = $this->dom->createElement("vCOFINS", $this->vCOFINS);
        $icmstot->appendChild($vCOFINS3);
        $vOutro     = $this->dom->createElement("vOutro", $this->vOutro);
        $icmstot->appendChild($vOutro);
        $vNF        = $this->dom->createElement("vNF", $this->vNF);
        $icmstot->appendChild($vNF);

        $transp     = $this->dom->createElement("transp");
        $infNFe->appendChild($transp);
        $modFrete   = $this->dom->createElement("modFrete", $this->modFrete);
        $transp->appendChild($modFrete);

        $pag        = $this->dom->createElement("pag");
        $infNFe->appendChild($pag);
        $tPag       = $this->dom->createElement("tPag", $this->tPag);
        $pag->appendChild($tPag);
        $vPag       = $this->dom->createElement("vPag", $this->vPag);
        $pag->appendChild($vPag);

        $infAdic    = $this->dom->createElement("infNFeSupl");
        $infNFe->appendChild($infAdic);
        $cdata = $this->dom->createCDATASection($this->infCpl);
        $infCpl     = $this->dom->createElement("qrCode");
        $infCpl->appendChild($cdata);
        $infAdic->appendChild($infCpl);

        $protNFe    = $this->dom->createElement("protNFe");
        $protNFe->setAttribute("versao", "3.10");

        $infProt    = $this->dom->createElement("infProt");
        $protNFe->appendChild($infProt);
        $tpAmb2     = $this->dom->createElement("tpAmb", $this->tpAmb);
        $infProt->appendChild($tpAmb2);
        $verAplic   = $this->dom->createElement("verAplic", $this->verAplic);
        $infProt->appendChild($verAplic);
        $chNFe      = $this->dom->createElement("chNFe", $this->chNFe);
        $infProt->appendChild($chNFe);
        $dhRecbto   = $this->dom->createElement("dhRecbto", $this->dhRecbto);
        $infProt->appendChild($dhRecbto);
        $nProt      = $this->dom->createElement("nProt", $this->nProt);
        $infProt->appendChild($nProt);
        $digVal     = $this->dom->createElement("digVal", $this->digVal);
        $infProt->appendChild($digVal);
        $cStat      = $this->dom->createElement("cStat", "100");
        $infProt->appendChild($cStat);
        $xMotivo    = $this->dom->createElement("xMotivo", $this->xMotivo);
        $infProt->appendChild($xMotivo);

        $nfe->appendChild($infNFe);

        $nfeProc->appendChild($nfe);
        $nfeProc->appendChild($protNFe);

        $this->dom->appendChild($nfeProc);
    }

    public function getNfce()
    {
        try {
            $this->pEmitente();
            $this->pDestinatario();
            $this->pNFe();
            $this->pProdutos();
            $this->pTotais();
            $this->pTransporte();
            $this->pCobranca();
            $this->pInfo();

            $this->parse();

            return $this->dom->saveXML();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function debug()
    {
        $this->pEmitente();
        $this->pDestinatario();
        $this->pNFe();
        $this->pProdutos();
        $this->pTotais();
        $this->pTransporte();
        $this->pCobranca();
        $this->pInfo();

        return $this;
    }
}