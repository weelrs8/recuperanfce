@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="panel-heading">
                        Nova Tarefa
                        <div class="pull-right">
                            <a href="{{ url('nfce') }}" class="btn btn-xs btn-default">Voltar</a>
                        </div>
                    </div>

                    <div class="panel-body">
                        @if ($uf)
                        <form method="post" action="{{ url('nfce/add') }}" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label class="control-label">Descrição</label>
                                <input type="text" name="name" class="form-control" required>
                            </div>
                            @if ($modo == 'planilha')
                                <div class="form-group">
                                    <label class="control-label">Planilha</label>
                                    <input type="file" name="file" class="form-control" accept=".xls, application/vnd.ms-excel">
                                </div>
                            @else
                                <div class="form-group">
                                    <label class="control-label">Chave de Acesso</label>
                                    <textarea name="keys" rows="10" class="form-control"></textarea>
                                </div>
                            @endif
                            <button type="submit" class="btn btn-primary btn-block">Enviar</button>
                        </form>
                        @endif
                    </div>
                </div>
                @if (null == $uf)
                    <div class="modal fade" tabindex="-1" role="dialog" id="myModal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Escolha um <strong>estado</strong> e um <strong>modo</strong> para continuar</h4>
                                </div>
                                <div class="modal-body">
                                    <div id="uf">
                                        <div class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-primary active">
                                                <input type="radio" name="uf" value="mt" autocomplete="off" checked> MT
                                            </label>
                                        </div>
                                    </div>
                                    <div id="modo">
                                        <hr>
                                        <div class="btn-group btn-group-justified" data-toggle="buttons">
                                            <label class="btn btn-primary active"x>
                                                <input type="radio" name="modo" value="planilha" autocomplete="off" checked> Planilha
                                            </label>
                                            <label class="btn btn-primary">
                                                <input type="radio" name="modo" value="digitado" autocomplete="off"> Digitado
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                    <a href="#!" id="mdContinuar" class="btn btn-primary" onclick="generateUrl()">Continuar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(window).load(function() {
            var uf =  "{{ $uf }}";
            if (uf.length == 0)
                $('#myModal').modal('show');
        });

        function generateUrl() {
            var uf = $("input[name='uf']:checked").val();
            var md = $("input[name='modo']:checked").val();

            $('#mdContinuar').attr("href", '/nfce/new/' + uf + '/' + md);
        }
    </script>
@endsection