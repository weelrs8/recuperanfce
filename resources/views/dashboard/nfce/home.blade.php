@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if (Session::has('status'))
                    <div class="alert alert-success">{{ Session::get('status') }}</div>
                @endif

                @if (Session::has('failure'))
                    <div class="alert alert-danger">{{ Session::get('failure') }}</div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span style="font-size: 16px">/// Tarefas</span>
                        <div class="pull-right">
                            <a href="{{ url('nfce/new') }}" class="btn btn-xs btn-primary">Cadastrar</a>
                        </div>
                    </div>

                    <table class="table table-striped">
                        <thead>
                            <tr style="font-weight: bold; font-size: 12px; text-transform:uppercase">
                                <td width="100">#</td>
                                <td>Descrição</td>
                                <td width="140">Etapa</td>
                                <td width="140">Status</td>
                                <td width="140">Criado</td>
                                <td width="140">Atualizado</td>
                                <td width="110" align="center">Ações</td>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($tasks) == 0)
                                <tr>
                                    <td colspan="7" align="center">Nenhum registro encontrado!</td>
                                </tr>
                            @endif
                            @foreach ($tasks as $t)
                            <tr>
                                <td>{{ $t->id }}</td>
                                <td style="cursor: pointer" onclick="window.location='{{ url('nfce/keys', ['id' => $t->id]) }}'">
                                    {{ $t->name }}
                                </td>
                                <td>{{ $step[$t->id]['step'] }} / {{ $step[$t->id]['total'] }}</td>
                                <td>{{ $status[$t->id] }}</td>
                                <td>{{ date('d/m/Y H:i', strtotime($t->created_at)) }}</td>
                                <td>{{ date('d/m/Y H:i', strtotime($t->updated_at)) }}</td>
                                <td align="center">
                                    <form method="post" action="{{ url('nfce/file', ['id' => $t->id]) }}">
                                        <a href="{{ url('nfce/file', ['id' => $t->id]) }}">
                                            <i class="fa fa-cloud-download"></i>
                                        </a>&nbsp;
                                        <button type="submit" class="btn-link">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection