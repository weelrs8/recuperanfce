@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="panel-heading">
                        Nova Tarefa
                        <div class="pull-right">
                            <a href="{{ url('nfce') }}" class="btn btn-xs btn-default">Voltar</a>
                        </div>
                    </div>

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <td>Chave de Acesso</td>
                                <td width="110" align="center">Status</td>
                                <td width="110" align="center">Tentativas</td>
                                <td width="140">Criado</td>
                                <td width="140">Atualizado</td>
                                <td width="60" align="center">Ações</td>
                            </tr>
                            </thead>
                            <tbody style="font-size: 12px">
                            @if (count($keys) == 0)
                                <tr>
                                    <td colspan="5" align="center">Nenhum registro encontrado!</td>
                                </tr>
                            @endif
                            @foreach ($keys as $k)
                                <tr>
                                    <td>{{ $k->key }}</td>
                                    <td align="center">
                                        @if ($k->status == 0)
                                            <label class="label label-warning">Na fila</label>
                                        @elseif ($k->status == 1)
                                            <label class="label label-info">Processando</label>
                                        @elseif ($k->status == 2)
                                            <label class="label label-danger">Error</label>
                                        @elseif ($k->status == 3)
                                            <label class="label label-success">Completo</label>
                                        @endif
                                    </td>
                                    <td align="center">{{ $k->attempts }} / 5</td>
                                    <td>{{ date('d/m/Y H:i', strtotime($k->created_at)) }}</td>
                                    <td>{{ date('d/m/Y H:i', strtotime($k->updated_at)) }}</td>
                                    <td align="center"><i class="glyphicon glyphicon-download"></i></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
@endsection